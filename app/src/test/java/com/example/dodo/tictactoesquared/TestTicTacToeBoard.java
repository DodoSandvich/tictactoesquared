package com.example.dodo.tictactoesquared;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class TestTicTacToeBoard {

    TicTacToeBoard board;

    @Before
    public void setUp(){
        board = new TicTacToeBoard();
    }

    @Test
    public void topRightStartsAsNone(){
        assertThat(board.getCell(CellPosition.TOPRIGHT), is(Player.NONE));
    }

    @Test
    public void winnerStartsAsNone(){
        assertThat(board.getWinner(), is(Player.NONE));
    }

    @Test
    public void placingOnTopRightAsXreturnsX() throws AlreadyTakenException {
        board.move(Player.X, CellPosition.TOPRIGHT);
        assertThat(board.getCell(CellPosition.TOPRIGHT), is(Player.X));
    }

    @Test(expected = AlreadyTakenException.class)
    public void placingTwiceReturnsAlreadyTakenException() {
        board.move(Player.X, CellPosition.TOPRIGHT);
        board.move(Player.X, CellPosition.TOPRIGHT);
    }

    @Test(expected = InvalidPlayerException.class)
    public void placingAsNoneReturnsInvalidPlayerException() {
        board.move(Player.NONE, CellPosition.TOPRIGHT);
    }

    @Test
    public void topRowXWins(){
        board.move(Player.X, CellPosition.TOPLEFT);
        board.move(Player.X, CellPosition.TOPMIDDLE);
        board.move(Player.X, CellPosition.TOPRIGHT);
        assertThat(board.getWinner(), is(Player.X));
    }

    @Test
    public void noWinner(){
        board.move(Player.X, CellPosition.TOPLEFT);
        board.move(Player.X, CellPosition.TOPMIDDLE);
        board.move(Player.X, CellPosition.BOTTOMLEFT);
        board.move(Player.O, CellPosition.TRUEMIDDLE);
        board.move(Player.O, CellPosition.MIDDLERIGHT);
        board.move(Player.O, CellPosition.BOTTOMRIGHT);
        assertThat(board.getWinner(), is(Player.NONE));
    }

    @Test(expected = GameFinishedException.class)
    public void gameFinishedMove(){
        board.move(Player.X, CellPosition.TOPLEFT);
        board.move(Player.X, CellPosition.TOPMIDDLE);
        board.move(Player.X, CellPosition.TOPRIGHT);
        board.move(Player.O, CellPosition.MIDDLERIGHT);
    }
}
