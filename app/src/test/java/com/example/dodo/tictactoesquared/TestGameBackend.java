package com.example.dodo.tictactoesquared;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class TestGameBackend {

    GameBackend game;

    @Before
    public void setUp(){
        game = new GameBackend();
    }

    @Test
    public void firstPlayerIsX(){
        assertThat(game.getPlayerInTurn(), is(Player.X));
    }

    @Test
    public void secondPlayerIsY(){
        game.nextTurn();
        assertThat(game.getPlayerInTurn(), is(Player.O));
    }

}
