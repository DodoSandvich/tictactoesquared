package com.example.dodo.tictactoesquared;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class TestSquaredBoard {

    SquaredBoard board;

    @Before
    public void setUp(){
        board = new SquaredBoard();
    }

    @Test
    public void getSmallBoardWorks(){
        assertNotNull(board.getSmallBoard(CellPosition.TOPRIGHT));
    }

    @Test
    public void topRightTopRightStartsEmpty(){
        assertThat(board.getCell(CellPosition.TOPRIGHT, CellPosition.TOPRIGHT), is(Player.NONE));
    }

    @Test
    public void nextMoveRestrictionStartsAsAll(){
        assertThat(board.getNextMoveRestriction(), is((Position) AreaRestrictPosition.ALL));
    }

    @Test(expected = AlreadyTakenException.class)
    public void alreadyTakenExceptionWorks(){
        board.move(Player.X, CellPosition.TOPRIGHT, CellPosition.TOPRIGHT);
        board.move(Player.O, CellPosition.TOPRIGHT, CellPosition.TOPRIGHT);
    }

    @Test(expected = InvalidAreaException.class)
    public void invalidAreaWorks(){
        board.move(Player.X, CellPosition.TOPRIGHT, CellPosition.TOPRIGHT);
        board.move(Player.O, CellPosition.TOPLEFT, CellPosition.TOPRIGHT);
    }

    @Test
    public void filledSmallGivesAll(){
        board.move(Player.X, CellPosition.TOPRIGHT, CellPosition.TRUEMIDDLE);
        board.move(Player.O, CellPosition.TRUEMIDDLE, CellPosition.TOPRIGHT);
        board.move(Player.X, CellPosition.TOPRIGHT, CellPosition.MIDDLELEFT);
        board.move(Player.O, CellPosition.MIDDLELEFT, CellPosition.TOPRIGHT);
        board.move(Player.X, CellPosition.TOPRIGHT, CellPosition.MIDDLERIGHT);
        board.move(Player.O, CellPosition.MIDDLERIGHT, CellPosition.TOPRIGHT);
        assertThat(board.getNextMoveRestriction(), is((Position) AreaRestrictPosition.ALL));
    }

    @Test
    public void startsWithNoWinner(){
        assertThat(board.getWinner(), is(Player.NONE));
    }

    @Test
    public void winnerWorks(){
        board.move(Player.X, CellPosition.TOPRIGHT, CellPosition.TRUEMIDDLE); //---------
        board.move(Player.O, CellPosition.TRUEMIDDLE, CellPosition.TOPRIGHT); //XXXXXXXXX
        board.move(Player.X, CellPosition.TOPRIGHT, CellPosition.MIDDLELEFT); //---------
        board.move(Player.O, CellPosition.MIDDLELEFT, CellPosition.TOPRIGHT); //OOOOOOO-O
        board.move(Player.X, CellPosition.TOPRIGHT, CellPosition.MIDDLERIGHT);//---------
        board.move(Player.O, CellPosition.MIDDLERIGHT, CellPosition.TOPRIGHT);//---------

        board.move(Player.X, CellPosition.TOPLEFT, CellPosition.TRUEMIDDLE);  //---------
        board.move(Player.O, CellPosition.TRUEMIDDLE, CellPosition.TOPLEFT);  //---------
        board.move(Player.X, CellPosition.TOPLEFT, CellPosition.MIDDLELEFT);  //---------
        board.move(Player.O, CellPosition.MIDDLELEFT, CellPosition.TOPLEFT);
        board.move(Player.X, CellPosition.TOPLEFT, CellPosition.MIDDLERIGHT);
        board.move(Player.O, CellPosition.MIDDLERIGHT, CellPosition.TOPLEFT);

        board.move(Player.X, CellPosition.TOPMIDDLE, CellPosition.TRUEMIDDLE);
        board.move(Player.O, CellPosition.TRUEMIDDLE, CellPosition.TOPMIDDLE);
        board.move(Player.X, CellPosition.TOPMIDDLE, CellPosition.MIDDLELEFT);
        board.move(Player.O, CellPosition.MIDDLELEFT, CellPosition.TOPMIDDLE);
        board.move(Player.X, CellPosition.TOPMIDDLE, CellPosition.MIDDLERIGHT);

        assertThat(board.getWinner(), is(Player.X));
    }

    @Test(expected = GameFinishedException.class)
    public void gameFinishedException(){
        board.move(Player.X, CellPosition.TOPRIGHT, CellPosition.TRUEMIDDLE); //---------
        board.move(Player.O, CellPosition.TRUEMIDDLE, CellPosition.TOPRIGHT); //XXXXXXXXX
        board.move(Player.X, CellPosition.TOPRIGHT, CellPosition.MIDDLELEFT); //---------
        board.move(Player.O, CellPosition.MIDDLELEFT, CellPosition.TOPRIGHT); //OOOOOOO-O
        board.move(Player.X, CellPosition.TOPRIGHT, CellPosition.MIDDLERIGHT);//---------
        board.move(Player.O, CellPosition.MIDDLERIGHT, CellPosition.TOPRIGHT);//---------

        board.move(Player.X, CellPosition.TOPLEFT, CellPosition.TRUEMIDDLE);  //---------
        board.move(Player.O, CellPosition.TRUEMIDDLE, CellPosition.TOPLEFT);  //---------
        board.move(Player.X, CellPosition.TOPLEFT, CellPosition.MIDDLELEFT);  //---------
        board.move(Player.O, CellPosition.MIDDLELEFT, CellPosition.TOPLEFT);
        board.move(Player.X, CellPosition.TOPLEFT, CellPosition.MIDDLERIGHT);
        board.move(Player.O, CellPosition.MIDDLERIGHT, CellPosition.TOPLEFT);

        board.move(Player.X, CellPosition.TOPMIDDLE, CellPosition.TRUEMIDDLE);
        board.move(Player.O, CellPosition.TRUEMIDDLE, CellPosition.TOPMIDDLE);
        board.move(Player.X, CellPosition.TOPMIDDLE, CellPosition.MIDDLELEFT);
        board.move(Player.O, CellPosition.MIDDLELEFT, CellPosition.TOPMIDDLE);
        board.move(Player.X, CellPosition.TOPMIDDLE, CellPosition.MIDDLERIGHT);

        board.move(Player.O, CellPosition.MIDDLERIGHT, CellPosition.TOPMIDDLE);
    }
}
