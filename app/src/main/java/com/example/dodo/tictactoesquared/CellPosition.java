package com.example.dodo.tictactoesquared;

public enum CellPosition implements Position {
    TOPLEFT,TOPMIDDLE,TOPRIGHT,MIDDLELEFT,TRUEMIDDLE, MIDDLERIGHT, BOTTOMLEFT, BOTTOMMIDDLE, BOTTOMRIGHT
}
