package com.example.dodo.tictactoesquared;

public class TicTacToeBoard {

    SimpleBoard board;

    public TicTacToeBoard(){
        board = new SimpleBoard();
    }

    public TicTacToeBoard(SimpleBoard board){
        this.board = board;
    }

    public void move(Player player, CellPosition cellPosition){
        if(getCell(cellPosition) != Player.NONE){
            throw new AlreadyTakenException();
        }
        if(player == Player.NONE){
            throw new InvalidPlayerException();
        }
        if(getWinner() != Player.NONE){
            throw new GameFinishedException();
        }
        board.changeCell(player, cellPosition);
    }

    public Player getCell(CellPosition cellPosition){
        return board.getCell(cellPosition);
    }

    public Player getWinner(){
        return board.getWinner();
    }
}
