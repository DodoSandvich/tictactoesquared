package com.example.dodo.tictactoesquared;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.GridLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class GameActivity extends AppCompatActivity {

    //game
    GameBackend gameBackend;
    GameBackend oldGame;

    static final String TAG = "GameActivity";
    static final String BOARDSTATE = "BoardState_";
    static final String PLAYERINTURNSTATE = "PlayerInTurnState";
    static final String RESTRICTIONSTATE = "RestrictionState";

    //board
    int ROWS = 3;
    int COLUMNS = 3;
    HashMap<CellPosition, GridLayout> squareLayouts;
    ConstraintLayout boardParent;

    ArrayList<CellButton> buttons;

    CellPosition selectedSquare, selectedSmall;

    //SharedPrefs
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor spEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        buttons = new ArrayList<>();

        boardParent = findViewById(R.id.BoardParent);

        squareLayouts = new HashMap<>();

        squareLayouts.put(CellPosition.TOPLEFT, (GridLayout) findViewById(R.id.BoardTopLeft));
        squareLayouts.put(CellPosition.TOPMIDDLE, (GridLayout) findViewById(R.id.BoardTopMiddle));
        squareLayouts.put(CellPosition.TOPRIGHT, (GridLayout) findViewById(R.id.BoardTopRight));
        squareLayouts.put(CellPosition.MIDDLELEFT, (GridLayout) findViewById(R.id.BoardMiddleLeft));
        squareLayouts.put(CellPosition.TRUEMIDDLE, (GridLayout) findViewById(R.id.BoardTrueMiddle));
        squareLayouts.put(CellPosition.MIDDLERIGHT, (GridLayout) findViewById(R.id.BoardMiddleRight));
        squareLayouts.put(CellPosition.BOTTOMLEFT, (GridLayout) findViewById(R.id.BoardBottomLeft));
        squareLayouts.put(CellPosition.BOTTOMMIDDLE, (GridLayout) findViewById(R.id.BoardBottomMiddle));
        squareLayouts.put(CellPosition.BOTTOMRIGHT, (GridLayout) findViewById(R.id.BoardBottomRight));

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        spEditor = sharedPreferences.edit();

        final Context context = this;
        final GameActivity gameActivity = this;

        for (final CellPosition square:
             CellPosition.values()) {
            final GridLayout boardLayout = squareLayouts.get(square);
            boardLayout.setRowCount(ROWS);
            boardLayout.setColumnCount(COLUMNS);
            boardLayout.setOrientation(GridLayout.HORIZONTAL);

            boardLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int buttonSize = (boardLayout.getWidth()/COLUMNS);
                    //Create our buttons

                    for (CellPosition small:
                         CellPosition.values()) {
                        CellButton cellButton = new CellButton(context, gameActivity, square, small);
                        cellButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                CellButton me = (CellButton) view;
                                CellPosition square = me.getSquare();
                                CellPosition small = me.getSmall();
                                setSelectedSquareAndSmall(square, small);
                                updateButtons();
                            }
                        });
                        cellButton.setLayoutParams(new ViewGroup.LayoutParams(buttonSize, buttonSize));
                        boardLayout.addView(cellButton);
                        buttons.add(cellButton);
                    }

                    //Only do this the first time the gridlayout updates
                    boardLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        gameBackend = getBoardFromSharedPrefs(1);
        oldGame = getBoardFromSharedPrefs(2);
        updateButtons();
    }

    @Override
    protected void onStop() {
        super.onStop();

        saveBoardToSharedPrefs(gameBackend, 1);
        saveBoardToSharedPrefs(oldGame, 2);
    }

    public void saveBoardToSharedPrefs(GameBackend board, int boardNumber){

        spEditor.putInt(PLAYERINTURNSTATE + boardNumber, board.getPlayerInTurnAsInt());
        spEditor.putInt(RESTRICTIONSTATE + boardNumber, board.getRestrictionAsInt());

        HashMap<Integer, HashMap<Integer, Integer>> boardAsInt = board.getBoardAsIntegers();

        int BOARDSIZE = 9;
        for (int x = 0; x < BOARDSIZE; x++) {
            for (int y = 0; y < BOARDSIZE; y++) {
                int cellOwner = boardAsInt.get(x).get(y);
                spEditor.putInt(BOARDSTATE + x + y + boardNumber, cellOwner);
            }
        }

        spEditor.apply();
        Log.d(TAG, "saveBoardToSharedPrefs: Saved " + board.getPlayerInTurnAsInt() + " " + board.getRestrictionAsInt() + " " + boardNumber);
        printBoard(board);
    }

    public GameBackend getBoardFromSharedPrefs(int boardNumber){
        int playerInTurnAsInt = sharedPreferences.getInt(PLAYERINTURNSTATE + boardNumber, 1);
        int restrictionAsInt = sharedPreferences.getInt(RESTRICTIONSTATE + boardNumber, 10);

        HashMap<Integer, HashMap<Integer, Integer>> boardAsInt = new HashMap<>();
        for (int x = 0; x < 9; x++) {
            HashMap<Integer, Integer> column = new HashMap<>();
            for (int y = 0; y < 9; y++) {
                column.put(y, sharedPreferences.getInt(BOARDSTATE + x + y + boardNumber, 0));
            }
            boardAsInt.put(x, column);
        }

        Log.d(TAG, "getBoardFromSharedPrefs: Loaded " + playerInTurnAsInt + " " + restrictionAsInt + " " + boardNumber);
        GameBackend result = new GameBackend(boardAsInt, playerInTurnAsInt, restrictionAsInt);
        printBoard(result);
        return result;
    }

    public void updateButtons(){
        for (CellButton button:
             buttons) {
            button.updateCell();
        }
        for (CellPosition position: CellPosition.values()){
            GridLayout squareLayout = squareLayouts.get(position);
            if(getSquare(position) == Player.X){
                squareLayout.setBackgroundColor(Color.BLUE);
            } else if (getSquare(position) == Player.O){
                squareLayout.setBackgroundColor(Color.RED);
            } else {
                squareLayout.setBackgroundColor(Color.TRANSPARENT);
            }
        }
        if(gameBackend.getWinner() == Player.O){
            boardParent.setBackgroundColor(Color.RED);
        } else if(gameBackend.getWinner() == Player.X){
            boardParent.setBackgroundColor(Color.BLUE);
        } else {
            boardParent.setBackgroundColor(Color.BLACK);
        }
    }

    public void performMove(CellPosition squared, CellPosition small){
        try {
            gameBackend.move(squared, small);
        } catch (AlreadyTakenException e){
            Toast.makeText(this, getString(R.string.e_already_taken), Toast.LENGTH_SHORT).show();
        } catch (InvalidAreaException e){
            Toast.makeText(this, getString(R.string.e_outside_restricted), Toast.LENGTH_SHORT).show();
        } catch (SquareFinishedException e){
            Toast.makeText(this, getString(R.string.e_square_done), Toast.LENGTH_SHORT).show();
        } catch (GameFinishedException e){
            Toast.makeText(this, getString(R.string.e_game_done), Toast.LENGTH_SHORT).show();
        }
        updateButtons();
    }

    public Player getSquare(CellPosition square){
        return gameBackend.getSquare(square);
    }

    public Player getCell(CellPosition square, CellPosition small){
        return gameBackend.getCell(square, small);
    }

    public void setSelectedSquareAndSmall(CellPosition selectedSquare, CellPosition selectedSmall) {
        if(getWinner() != Player.NONE){
            Toast.makeText(this, getString(R.string.e_game_done), Toast.LENGTH_SHORT).show();
            return;
        }
        if(getSquare(selectedSquare) != Player.NONE){
            Toast.makeText(this, getString(R.string.e_square_done), Toast.LENGTH_SHORT).show();
            return;
        }
        if(getCell(selectedSquare, selectedSmall) != Player.NONE){
            Toast.makeText(this, getString(R.string.e_already_taken), Toast.LENGTH_SHORT).show();
            return;
        }
        if(getRestriction() != AreaRestrictPosition.ALL && getRestriction() != selectedSquare){
            Toast.makeText(this, getString(R.string.e_outside_restricted), Toast.LENGTH_SHORT).show();
            return;
        }
        this.selectedSquare = selectedSquare;
        this.selectedSmall = selectedSmall;
    }

    public Position getRestriction(){
        return gameBackend.getRestriction();
    }

    public Player getWinner(){
        return gameBackend.getWinner();
    }

    public Player getPlayerInTurn(){
        return gameBackend.getPlayerInTurn();
    }

    public CellPosition getSelectedSmall() {
        return selectedSmall;
    }

    public CellPosition getSelectedSquare() {
        return selectedSquare;
    }

    public void newGamePress(View view) {
        newGame();
    }

    public void newGame(){
        //Perhaps make this cleaner.
        oldGame = gameBackend;
        gameBackend = new GameBackend();
        updateButtons();
        Toast.makeText(this, getString(R.string.save_board), Toast.LENGTH_SHORT).show();
    }

    public void playSelected(View view) {
        if(selectedSmall == null || selectedSquare == null){
            Toast.makeText(this, getString(R.string.e_no_selected), Toast.LENGTH_SHORT).show();
            return;
        }
        performMove(selectedSquare, selectedSmall);
        selectedSquare = null;
        selectedSmall = null;
    }

    public void loadGamePress(View view) {
        loadGame();
    }

    public void loadGame(){
        GameBackend temp = gameBackend;
        gameBackend = oldGame;
        oldGame = temp;
        updateButtons();
        Toast.makeText(this, getString(R.string.load_board), Toast.LENGTH_SHORT).show();
    }

    public void printBoard(GameBackend board){
        HashMap<Integer, HashMap<Integer, Integer>> boardAsInt = board.getBoardAsIntegers();
        for (int y = 0; y < 9; y++) {
            StringBuilder line = new StringBuilder();
            for (int x = 0; x < 9; x++) {
                line.append(boardAsInt.get(x).get(y));
            }
            Log.d(TAG, "printBoard: " + line.toString());
        }
    }
}
