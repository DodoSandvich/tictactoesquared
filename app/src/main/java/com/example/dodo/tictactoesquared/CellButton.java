package com.example.dodo.tictactoesquared;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

public class CellButton extends View {

    final static int MARGIN_SIZE = 6;
    final static int CROSS_SIZE = 14;
    final static int CIRCLE_SIZE = 16;
    final static int ANIMATION_DURATION = 500;

    GameActivity gameActivity;
    Player owner, squareWinner;
    Paint neutralPaint, xPaint, oPaint, xWinPaint, oWinPaint, animationPaint;
    Path crossPathOne, crossPathTwo;
    int restrictionPaintInt, selectPaintInt, animationPaintInt;

    ObjectAnimator highlightAnimator;

    CellPosition square, small;
    boolean selected;
    boolean inRestriction;

    Context context;



    public CellButton(Context context, GameActivity gameActivity, CellPosition square, CellPosition small) {
        super(context);
        this.context = context;
        this.gameActivity = gameActivity;
        this.square = square;
        this.small = small;
        neutralPaint = new Paint();
        neutralPaint.setColor(ContextCompat.getColor(context, R.color.background));
        xWinPaint = new Paint();
        xWinPaint.setColor(ContextCompat.getColor(context, R.color.xWin));
        oWinPaint = new Paint();
        oWinPaint.setColor(ContextCompat.getColor(context, R.color.oWin));

        xPaint = new Paint();
        xPaint.setColor(ContextCompat.getColor(context, R.color.xNormal));
        xPaint.setStyle(Paint.Style.FILL);
        oPaint = new Paint();
        oPaint.setColor(ContextCompat.getColor(context, R.color.oNormal));
        animationPaint = new Paint();

        restrictionPaintInt = ContextCompat.getColor(context, R.color.restriction);
        selectPaintInt = ContextCompat.getColor(context, R.color.select);
        animationPaintInt = restrictionPaintInt;

        crossPathOne = new Path();
        crossPathTwo = new Path();

        highlightAnimator = ObjectAnimator.ofObject(this, "animationPaint", new ArgbEvaluator(), restrictionPaintInt, selectPaintInt);
        highlightAnimator.setRepeatMode(ValueAnimator.REVERSE);
        highlightAnimator.setRepeatCount(ValueAnimator.INFINITE);
        highlightAnimator.setDuration(ANIMATION_DURATION);

        updateCell();
    }

    public void updateCell(){
        owner = gameActivity.getCell(square, small);
        squareWinner = gameActivity.getSquare(square);
        Position restriction = gameActivity.getRestriction();
        inRestriction = (restriction == AreaRestrictPosition.ALL || restriction == square) && gameActivity.getSquare(square) == Player.NONE && gameActivity.getWinner() == Player.NONE;
        selected = gameActivity.getSelectedSmall() == small && gameActivity.getSelectedSquare() == square;

        if(selected){
            if(!highlightAnimator.isRunning()){
                highlightAnimator.start();
            }
        } else {
            highlightAnimator.end();
            animationPaintInt = restrictionPaintInt;
        }

        invalidate();
    }

    public void setAnimationPaint(int animationPaint) {
        this.animationPaintInt = animationPaint;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width = canvas.getWidth();
        int height = canvas.getHeight();

        crossPathOne.reset();
        crossPathOne.moveTo(MARGIN_SIZE, CROSS_SIZE);
        crossPathOne.lineTo(CROSS_SIZE, MARGIN_SIZE);
        crossPathOne.lineTo(width-MARGIN_SIZE, height-CROSS_SIZE);
        crossPathOne.lineTo(width-CROSS_SIZE, height-MARGIN_SIZE);
        crossPathOne.lineTo(MARGIN_SIZE, CROSS_SIZE);

        crossPathTwo.reset();
        crossPathTwo.moveTo(MARGIN_SIZE,height-CROSS_SIZE);
        crossPathTwo.lineTo(width-CROSS_SIZE, MARGIN_SIZE);
        crossPathTwo.lineTo(width-MARGIN_SIZE, CROSS_SIZE);
        crossPathTwo.lineTo(CROSS_SIZE, height-MARGIN_SIZE);
        crossPathTwo.lineTo(MARGIN_SIZE, height-CROSS_SIZE);

        Paint backgroundPaint;
        if(squareWinner == Player.NONE){
            backgroundPaint = neutralPaint;
        } else if (squareWinner == Player.O){
            backgroundPaint = oWinPaint;
        } else {
            backgroundPaint = xWinPaint;
        }
        canvas.drawRect(MARGIN_SIZE, MARGIN_SIZE, width-MARGIN_SIZE, height-MARGIN_SIZE, backgroundPaint);

        if(owner == Player.X){
            canvas.drawPath(crossPathOne, xPaint);
            canvas.drawPath(crossPathTwo, xPaint);
        } else if(owner == Player.O){
            canvas.drawCircle(width/2f, height/2f, width/2f-MARGIN_SIZE, oPaint);
            canvas.drawCircle(width/2f, height/2f, width/2f-CIRCLE_SIZE, backgroundPaint);
        } else if(inRestriction){
            animationPaint.setColor(animationPaintInt);
          if(gameActivity.getPlayerInTurn() == Player.X){
              canvas.drawPath(crossPathOne, animationPaint);
              canvas.drawPath(crossPathTwo, animationPaint);
          } else {
              canvas.drawCircle(width/2, height/2, width/2-MARGIN_SIZE, animationPaint);
              canvas.drawCircle(width/2, height/2, width/2-CIRCLE_SIZE, backgroundPaint);
          }
        }
    }

    public CellPosition getSquare() {
        return square;
    }

    public CellPosition getSmall() {
        return small;
    }

    //Unneeded constructors
    public CellButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CellButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CellButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
