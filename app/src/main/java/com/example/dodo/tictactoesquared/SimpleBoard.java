package com.example.dodo.tictactoesquared;

import java.util.HashMap;

public class SimpleBoard {
    HashMap<CellPosition, Player> cells = new HashMap<>();
    Player winner;

    public SimpleBoard(){
        for (CellPosition cellPosition : CellPosition.values()) {
            cells.put(cellPosition, Player.NONE);
        }
        winner = Player.NONE;
    }

    public void changeCell(Player player, CellPosition cellPosition){
        cells.put(cellPosition, player);
        calculateWinner();
    }

    public Player getCell(CellPosition cellPosition){
        return cells.get(cellPosition);
    }

    public void calculateWinner(){
        Player topLeft = getCell(CellPosition.TOPLEFT);
        if(topLeft != Player.NONE){
            if(topLeft == getCell(CellPosition.TOPMIDDLE) && topLeft == getCell(CellPosition.TOPRIGHT)){
                winner = topLeft;
                return;
            }
            if(topLeft == getCell(CellPosition.MIDDLELEFT) && topLeft == getCell(CellPosition.BOTTOMLEFT)){
                winner = topLeft;
                return;
            }
        }
        Player middle = getCell(CellPosition.TRUEMIDDLE);
        if(middle != Player.NONE){
            if(middle == getCell(CellPosition.TOPLEFT) && middle == getCell(CellPosition.BOTTOMRIGHT)){
                winner = middle;
                return;
            }
            if(middle == getCell(CellPosition.MIDDLELEFT) && middle == getCell(CellPosition.MIDDLERIGHT)){
                winner = middle;
                return;
            }
            if(middle == getCell(CellPosition.BOTTOMLEFT) && middle == getCell(CellPosition.TOPRIGHT)){
                winner = middle;
                return;
            }
            if(middle == getCell(CellPosition.TOPMIDDLE) && middle == getCell(CellPosition.BOTTOMMIDDLE)) {
                winner = middle;
                return;
            }
        }
        Player bottomRight = getCell(CellPosition.BOTTOMRIGHT);
        if(bottomRight != Player.NONE){
            if(bottomRight == getCell(CellPosition.BOTTOMLEFT) && bottomRight == getCell(CellPosition.BOTTOMMIDDLE)){
                winner = bottomRight;
                return;
            }
            if(bottomRight == getCell(CellPosition.TOPRIGHT) && bottomRight == getCell(CellPosition.MIDDLERIGHT)){
                winner = bottomRight;
            }
        }
        //Nobody won yet, stay at none
    }

    public Player getWinner(){
        return winner;
    }
}
