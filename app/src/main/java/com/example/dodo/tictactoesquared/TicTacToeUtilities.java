package com.example.dodo.tictactoesquared;

public class TicTacToeUtilities {

    public static int playerToInt(Player player){
        switch (player){
            case X: return 1;
            case O: return 2;
            default: return 0;
        }
    }

    public static Player intToPlayer(int player){
        switch (player){
            case 1: return Player.X;
            case 2: return Player.O;
            default: return Player.NONE;
        }
    }
}
