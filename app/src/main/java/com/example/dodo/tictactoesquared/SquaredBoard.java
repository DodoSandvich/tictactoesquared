package com.example.dodo.tictactoesquared;

import java.util.HashMap;

import static com.example.dodo.tictactoesquared.AreaRestrictPosition.ALL;

public class SquaredBoard {

    HashMap<CellPosition, TicTacToeBoard> smallBoards;
    Position nextMoveRestriction;
    SimpleBoard masterBoard;

    public SquaredBoard(){
        smallBoards = new HashMap<>();
        for (CellPosition cellPosition :
             CellPosition.values()) {
            smallBoards.put(cellPosition, new TicTacToeBoard());
        }
        nextMoveRestriction = ALL;
        masterBoard = new SimpleBoard();
    }

    public SquaredBoard(HashMap<Integer, HashMap<Integer, Integer>> boardAsIntegers, int restrictionAsInt){
        smallBoards = new HashMap<>();
        for (CellPosition squarePosition :
                CellPosition.values()) {
            SimpleBoard squareBoard = new SimpleBoard();
            for(CellPosition smallPosition:
                    CellPosition.values()){
                Player cellOwner = TicTacToeUtilities.intToPlayer(boardAsIntegers.get(cellToX(squarePosition, smallPosition)).get(cellToY(squarePosition, smallPosition)));
                squareBoard.changeCell(cellOwner, smallPosition);
            }
            smallBoards.put(squarePosition, new TicTacToeBoard(squareBoard));
        }
        nextMoveRestriction = intToPosition(restrictionAsInt);
        masterBoard = new SimpleBoard();
        calculateWinner();
    }

    public TicTacToeBoard getSmallBoard(CellPosition cellPosition){
        return smallBoards.get(cellPosition);
    }

    public Player getCell(CellPosition squared, CellPosition small){
        return smallBoards.get(squared).getCell(small);
    }

    public Player getSquare(CellPosition squared){
        return masterBoard.getCell(squared);
    }

    public Position getNextMoveRestriction(){
        return nextMoveRestriction;
    }

    public void move(Player player, CellPosition squared, CellPosition small){
        if(getWinner() != Player.NONE){
            throw new GameFinishedException();
        }
        if(nextMoveRestriction != ALL && nextMoveRestriction != squared){
            throw new InvalidAreaException();
        }
        if(masterBoard.getCell(squared) != Player.NONE){
            throw new SquareFinishedException();
        }
        getSmallBoard(squared).move(player, small);
        if(getSmallBoard(small).getWinner() == Player.NONE){
            nextMoveRestriction = small;
        } else {
            nextMoveRestriction = ALL;
        }
        calculateWinner();
    }

    public void calculateWinner(){
        for (CellPosition position:
             CellPosition.values()) {
            Player smallBoardWinner = getSmallBoard(position).getWinner();
            masterBoard.changeCell(smallBoardWinner, position);
        }
    }

    public Player getWinner(){
        return masterBoard.getWinner();
    }

    public HashMap<Integer, HashMap<Integer, Integer>> getBoardAsIntegers(){
        HashMap<Integer, HashMap<Integer, Integer>> result = new HashMap<>();
        for (int x = 0; x < 9; x++) {
            HashMap<Integer, Integer> column = new HashMap<>();
            for (int y = 0; y < 9; y++) {
                Player cellOwner = getCell(convertIntToSquareCell(x, y), convertIntToSmallCell(x, y));
                int cellOwnerInt = TicTacToeUtilities.playerToInt(cellOwner);
                column.put(y, cellOwnerInt);
            }
            result.put(x, column);
        }
        return result;
    }

    public int getRestrictionAsInteger(){
        return positionToInt(getNextMoveRestriction());
    }

    public CellPosition convertIntToSquareCell(int x, int y){
        if(x <= 2){
            if(y <= 2){
                return CellPosition.TOPLEFT;
            } else if(y <= 5){
                return CellPosition.MIDDLELEFT;
            } else {
                return CellPosition.BOTTOMLEFT;
            }
        } else if(x <= 5){
            if(y <= 2){
                return CellPosition.TOPMIDDLE;
            } else if(y <= 5){
                return CellPosition.TRUEMIDDLE;
            } else {
                return CellPosition.BOTTOMMIDDLE;
            }
        } else {
            if(y <= 2){
                return CellPosition.TOPRIGHT;
            } else if(y <= 5){
                return CellPosition.MIDDLERIGHT;
            } else {
                return CellPosition.BOTTOMRIGHT;
            }
        }
    }

    public CellPosition convertIntToSmallCell(int x, int y){
        if(y == 0 || y == 3 || y == 6) {
            if (x == 0 || x == 3 || x == 6) {
                return CellPosition.TOPLEFT;
            } else if (x == 1 || x == 4 || x == 7) {
                return CellPosition.TOPMIDDLE;
            } else {
                return CellPosition.TOPRIGHT;
            }
        } else if (y == 1 || y == 4 || y == 7) {
            if (x == 0 || x == 3 || x == 6) {
                return CellPosition.MIDDLELEFT;
            } else if (x == 1 || x == 4 || x == 7) {
                return CellPosition.TRUEMIDDLE;
            } else {
                return CellPosition.MIDDLERIGHT;
            }
        } else {
            if (x == 0 || x == 3 || x == 6) {
                return CellPosition.BOTTOMLEFT;
            } else if (x == 1 || x == 4 || x == 7) {
                return CellPosition.BOTTOMMIDDLE;
            } else {
                return CellPosition.BOTTOMRIGHT;
            }
        }
    }

    public int cellToX(CellPosition square, CellPosition small){
        int res = 0;
        switch (square){
            case TOPLEFT: case MIDDLELEFT: case BOTTOMLEFT: break;
            case TOPMIDDLE: case TRUEMIDDLE: case BOTTOMMIDDLE: res+=3; break;
            case TOPRIGHT: case MIDDLERIGHT: case BOTTOMRIGHT: res+=6; break;
            default:break;
        }
        switch (small){
            case TOPLEFT: case MIDDLELEFT: case BOTTOMLEFT: break;
            case TOPMIDDLE: case TRUEMIDDLE: case BOTTOMMIDDLE: res+=1; break;
            case TOPRIGHT: case MIDDLERIGHT: case BOTTOMRIGHT: res+=2; break;
            default:break;
        }
        return res;
    }

    public int cellToY(CellPosition square, CellPosition small){
        int res = 0;
        switch (square){
            case TOPLEFT: case TOPMIDDLE: case TOPRIGHT: break;
            case MIDDLELEFT: case TRUEMIDDLE: case MIDDLERIGHT: res+=3; break;
            case BOTTOMLEFT: case BOTTOMMIDDLE: case BOTTOMRIGHT: res+=6; break;
            default: break;
        }
        switch (small){
            case TOPLEFT: case TOPMIDDLE: case TOPRIGHT: break;
            case MIDDLELEFT: case TRUEMIDDLE: case MIDDLERIGHT: res+=1; break;
            case BOTTOMLEFT: case BOTTOMMIDDLE: case BOTTOMRIGHT: res+=2; break;
            default: break;
        }
        return res;
    }

    public int positionToInt(Position pos){
        if(pos.getClass() != CellPosition.class){
            return 10;
        }
        CellPosition cellPos = (CellPosition) pos;
        switch (cellPos){
            case TOPLEFT: return 0;
            case TOPMIDDLE: return 1;
            case TOPRIGHT: return 2;
            case MIDDLELEFT: return 3;
            case TRUEMIDDLE: return 4;
            case MIDDLERIGHT: return 5;
            case BOTTOMLEFT: return 6;
            case BOTTOMMIDDLE: return 7;
            case BOTTOMRIGHT: return 8;
            default: return 10;
        }
    }

    public Position intToPosition(int position){
        switch(position){
            case 0: return CellPosition.TOPLEFT;
            case 1: return CellPosition.TOPMIDDLE;
            case 2: return CellPosition.TOPRIGHT;
            case 3: return CellPosition.MIDDLELEFT;
            case 4: return CellPosition.TRUEMIDDLE;
            case 5: return CellPosition.MIDDLERIGHT;
            case 6: return CellPosition.BOTTOMLEFT;
            case 7: return CellPosition.BOTTOMMIDDLE;
            case 8: return CellPosition.BOTTOMRIGHT;
            default: return AreaRestrictPosition.ALL;
        }
    }
}
