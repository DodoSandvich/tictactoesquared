package com.example.dodo.tictactoesquared;

import java.util.HashMap;

public class GameBackend {

    Player playerInTurn;
    SquaredBoard squaredBoard;

    public GameBackend(){
        playerInTurn = Player.X;
        squaredBoard = new SquaredBoard();
    }

    public GameBackend(HashMap<Integer, HashMap<Integer, Integer>> boardAsIntegers, int playerInTurnAsInt, int restrictionAsInt){
        playerInTurn = TicTacToeUtilities.intToPlayer(playerInTurnAsInt);
        squaredBoard = new SquaredBoard(boardAsIntegers, restrictionAsInt);
    }

    public Player getPlayerInTurn() {
        return playerInTurn;
    }

    public Position getRestriction(){
        return squaredBoard.getNextMoveRestriction();
    }

    public Player getCell(CellPosition squared, CellPosition small){
        return squaredBoard.getCell(squared, small);
    }

    public Player getWinner(){
        return squaredBoard.getWinner();
    }

    public Player getSquare(CellPosition squared){
        return squaredBoard.getSquare(squared);
    }

    public void move(CellPosition squared, CellPosition small){
        squaredBoard.move(playerInTurn, squared, small);
        nextTurn();
    }

    public void nextTurn(){
        if(playerInTurn == Player.X){
            playerInTurn = Player.O;
        } else {
            playerInTurn = Player.X;
        }
    }

    public HashMap<Integer, HashMap<Integer, Integer>> getBoardAsIntegers(){
        return squaredBoard.getBoardAsIntegers();
    }

    public int getPlayerInTurnAsInt(){
        return TicTacToeUtilities.playerToInt(playerInTurn);
    }

    public int getRestrictionAsInt(){
        return squaredBoard.getRestrictionAsInteger();
    }
}
